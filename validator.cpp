#include "validator.h"

Validator::Validator()
{
}

Validator::Result Validator::checkName(const QString& name)
{
    if (name.isEmpty())
    {
        return Result::IS_EMPTY;
    }
    QChar first = name[0];
    if (!first.isUpper())
    {
        return Result::FIRST_LETTER;
    }
    for (const auto& tmp : name)
    {
        if (tmp == " ")
        {
            return Result::HAVE_SPACE;
        }
        else if (!tmp.isLetter())
        {
            return Result::NOT_LETTER;
        }
    }
    return Result::OK;
}
