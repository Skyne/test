#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProxyStyle>
#include <QStyleOptionTab>
#include <QByteArray>
#include <QFile>
#include <QFontDatabase>
#include <QDebug>
#include <QStandardPaths>
#include <QDir>
#include <QMessageBox>
#include <QPicture>
#include <QStandardItemModel>
#include <QAbstractItemModel>
#include <QListWidget>

#include <QStyledItemDelegate>

#include "validator.h"
#include "dbconnector.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_tabWidget_tabBarDoubleClicked(int index);

    void on_pushButton_4_clicked();

    void on_tabWidget_currentChanged(int index);

    void on_pushButton_5_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
    DBConnector db_connector;
};

class MyListItem : public QListWidgetItem
{
public:
    QString *name;
    QString *country;
    QIcon *icon;
};

#endif // MAINWINDOW_H
