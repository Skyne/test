#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QItemEditorFactory>
#include <QStyledItemDelegate>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->tabWidget->setTabEnabled(0, false);

    ui->tabWidget->setCurrentIndex(1);
    QFile file(":/font/Roboto-Bold.ttf");
    file.open(QIODevice::ReadOnly);
    int a = QFontDatabase::addApplicationFontFromData(file.readAll());
    file.close();
    QString family = QFontDatabase::applicationFontFamilies(a).at(0);
    QFont font(family);
    this->setFont(font);
    font.setPixelSize(16);
    ui->label->setFont(font);
    ui->label_6->setFont(font);
    font.setPixelSize(14);
    ui->pushButton->setFont(font);
    ui->pushButton_2->setFont(font);
    ui->tab_2->setFont(font);
    ui->label->setFont(font);
    ui->tab_3->setFont(font);
    ui->label_2->setFont(font);
    ui->label_3->setFont(font);
    ui->label_4->setFont(font);
    ui->textEdit->setFont(font);
    ui->comboBox->setFont(font);
    ui->pushButton_4->setFont(font);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_tabWidget_tabBarDoubleClicked(int index)
{
    if (index != 0)
        ui->tabWidget->setCurrentIndex(index);
}

void MainWindow::on_pushButton_4_clicked()
{
    QString name = ui->textEdit->toPlainText();
    QString country = ui->comboBox->currentText();
    uint8_t type = 0;
    if (ui->radioButton->isChecked())
        type = 0;
    else if (ui->radioButton_2->isChecked())
        type = 1;
    else if (ui->radioButton_3->isChecked())
        type = 2;
    Validator::Result check = Validator::checkName(name);
    switch (check)
    {
    case Validator::Result::OK: {
        db_connector.add(name, country, type);
        break;
    }
    case Validator::Result::HAVE_SPACE: {
        QMessageBox::information(this, "Error", "Name must haven`t space");
        break;
    }
    case Validator::Result::NOT_LETTER: {
        QMessageBox::information(this, "Error", "Name must have only letter");
        break;
    }
    case Validator::Result::FIRST_LETTER: {
        QMessageBox::information(this, "Error", "First letter must be capital");
        break;
    }
    case Validator::Result::IS_EMPTY: {
        QMessageBox::information(this, "Error", "Name is empty");
        break;
    }
    }
}

void MainWindow::on_tabWidget_currentChanged(int index)
{
    QList<DBConnector::listRow> res = db_connector.getAll();
    QStandardItemModel *model = new QStandardItemModel();
    for (const auto &tmp : res)
    {
        QIcon icon;
        if (tmp.type == 0)
            icon.addFile(":/icons/avatar-1@3x.png");
        else if (tmp.type == 1)
            icon.addFile(":/icons/avatar-2@3x.png");
        else
            icon.addFile(":/icons/avatar-3@3x.png");
        QStandardItem *item = new QStandardItem();
        item->setData(tmp.index, Qt::StatusTipRole);
        item->setSizeHint(QSize(50, 50));
        item->setText(tmp.name);
        item->setIcon(icon);
        item->setData(tmp.country);
        model->appendRow(item);
    }
    ui->listView->setModel(model);
}

void MainWindow::on_pushButton_5_clicked()
{
    int index = ui->listView->currentIndex().data(Qt::StatusTipRole).toInt();
    if (index == -1)
    {
        QMessageBox::information(this, "Error", "Select account");
        return;
    }
    db_connector.deleteRow(index);

    on_tabWidget_currentChanged(0);
}

void MainWindow::on_pushButton_2_clicked()
{
    ui->tabWidget->setCurrentIndex(2);
}

void MainWindow::on_pushButton_clicked()
{
    ui->tabWidget->setCurrentIndex(1);
}
