#ifndef VALIDATOR_H
#define VALIDATOR_H
#include <QString>

class Validator
{
public:
    Validator();
    enum Result
    {
        OK,
        FIRST_LETTER,
        NOT_LETTER,
        HAVE_SPACE,
        IS_EMPTY
    };
    static Result checkName(const QString& name);
};

#endif // VALIDATOR_H
