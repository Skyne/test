#ifndef DB_CONNECTOR_H
#define DB_CONNECTOR_H
#include <QString>
#include <QSql>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QSqlError>
#include <QList>
#include <QDebug>

class DBConnector
{
public:
    struct listRow
    {
        int index;
        QString name;
        QString country;
        uint8_t type;
    };

    DBConnector();

    void add(const QString &name, const QString &country, const uint8_t &type);
    QList<listRow> getAll();
    void deleteRow(const int &index);

private:
    QSqlDatabase db;

    const QString DB_name = "DBASE";
    const QString DB_tableName = "Accounts";
};

#endif // DB_CONNECTOR_H
