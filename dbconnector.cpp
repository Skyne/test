#include "dbconnector.h"

DBConnector::DBConnector()
{
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(DB_name);
    QSqlQuery query;
    db.open();
    query.exec(QString("CREATE TABLE IF NOT EXISTS " + DB_tableName
                       + " ("
                         "accId        INTEGER PRIMARY KEY AUTOINCREMENT, "
                         "Name         TEXT        NOT NULL, "
                         "Nationality  TEXT        NOT NULL, "
                         "type         INT         NOT NULL ); "));
    db.close();
}

void DBConnector::add(const QString &name, const QString &country, const uint8_t &type)
{
    db.open();
    QSqlQuery query;
    query.prepare("INSERT INTO " + DB_tableName
                  + " (Name, Nationality, type)"
                    "VALUES (:Name, :Nationality, :type)");
    query.bindValue(":Name", name);
    query.bindValue(":Nationality", country);
    query.bindValue(":type", type);
    query.exec();
    db.close();
}

QList<DBConnector::listRow> DBConnector::getAll()
{
    QList<listRow> res;
    db.open();
    QSqlQuery query;
    query.exec("SELECT * FROM Accounts");
    while (query.next())
    {
        int index = query.value(0).toInt();
        QString country(query.value(2).toString());
        QString name(query.value(1).toString() + "\n" + country);
        uint8_t type = query.value(3).toInt();
        res.append({ index, name, country, type });
    }

    db.close();
    return res;
}

void DBConnector::deleteRow(const int &index)
{
    db.open();
    QSqlQuery query;
    query.exec("DELETE FROM Accounts Where accId = " + QString::number(index));
    db.close();
}
